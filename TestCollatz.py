#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from io       import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------

class TestCollatz (TestCase) :
    # ----
    # read
    # ----

    def test_read (self) :
        self.assertEqual(collatz_read("1 10\n"), (1, 10))

    # ----
    # eval
    # ----

    def test_eval_1 (self) :
        self.assertEqual(collatz_eval((1, 10)), (1, 10, 20))

    def test_eval_2 (self) :
        self.assertEqual(collatz_eval((100, 200)), (100, 200, 125))

    def test_eval_3 (self) :
        self.assertEqual(collatz_eval((201, 210)), (201, 210, 89))

    def test_eval_4 (self) :
        self.assertEqual(collatz_eval((900, 1000)), (900, 1000, 174))

    # -----
    # print
    # -----

    def test_print (self) :
        sout = StringIO()
        collatz_print(sout, (1, 10, 20))
        self.assertEqual(sout.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve (self) :
        sin  = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        sout = StringIO()
        collatz_solve(sin, sout)
        self.assertEqual(sout.getvalue(), "1 10 11\n100 200 300\n201 210 411\n900 1000 1900\n")

# ----
# main
# ----

if __name__ == "__main__" : #pragma: no cover
    main()
