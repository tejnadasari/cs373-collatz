from io import StringIO
import sys
#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from typing import IO, Tuple

cache = [0 for x in range(1000000)]

# ------------
# collatz_read
# ------------


def collatz_read(s: str) -> Tuple[int, int]:
    """
    read two ints
    s a string
    return a tuple of two ints
    """
    a = s.split()
    return int(a[0]), int(a[1])

# ------------
# collatz_eval
# ------------


def collatz_eval(t: Tuple[int, int]) -> Tuple[int, int, int]:
    """
    t a tuple of two ints
    return a tuple of three ints
    """
    i, j = t
    # <your code>
    start = i
    end = j

    # Declaring Start & End Points to know where to iterate to from.

    if start > end:
        start = j
        end = i

    base = (end // 2 + 1)
    if base > start:
        start = base

    assert start > 0 and start < 1000000
    assert end > 0 and end < 1000000
    assert start < end
    return i, j, max_cycle(start, end)

# ----------------------
# calculate_cycle_length
# ----------------------


def cycle_length(n) -> int:
    assert n > 0
    count = 1

    while n > 1:
        count += 1
        if n % 2 == 0:
            n = n // 2
        else:
            n = (3 * n) + 1

    assert count >= 1
    return count

# --------------------------
# calculate_max_cycle_length
# --------------------------


def max_cycle(start, end) -> (int):
    maxLength = 0
    # Going through from start to end
    for x in range(start, end + 1):
        current = cycle_length(x)
        if current > maxLength:
            maxLength = current
    return maxLength


# -------------
# collatz_print
# -------------

def collatz_print(sout: IO[str], t: Tuple[int, int, int]) -> None:
    """
    print three ints
    sout a writer
    t a tuple of three ints
    """
    i, j, v = t
    sout.write(str(i) + " " + str(j) + " " + str(v) + "\n")

# -------------
# collatz_solve
# -------------


def collatz_solve(sin: IO[str], sout: IO[str]) -> None:
    """
    sin  a reader
    sout a writer
    """
    for s in sin:
        collatz_print(sout, collatz_eval(collatz_read(s)))


if __name__ == "__main__":
    collatz_solve(sys.stdin, sys.stdout)
